# mingw-go

**Deprecated:** in favor of [go-cross-compile](https://git.openprivacy.ca/openprivacy/go-cross-compile) at [dockerhub/openpriv/go-cross-compile](https://hub.docker.com/r/openpriv/go-cross-compile)

versions: 

- 2024.01
  - go 1.21.5
- 2022.11
  - go 1.19.3
- 2022.09
  - go 1.17
- 2021.03
  - go 1.15

This container is based on [purplekarrot/mingw-w64-x86-64](https://hub.docker.com/r/purplekarrot/mingw-w64-x86-64) and then has Go 1.19.3 installed in /usr/local/go

Dockerfiles @ [git.openprivacy.ca/openprivacy/mingw-go](https://git.openprivacy.ca/openprivacy/mingw-go)
